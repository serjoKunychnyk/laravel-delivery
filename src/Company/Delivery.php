<?php

namespace UNWEB\LaravelDelivery\Company;

use UNWEB\LaravelDelivery\Models\DeliveryCity;
use UNWEB\LaravelDelivery\Models\DeliverySettlement;
use Illuminate\Support\Facades\Http;

class Delivery
{

    protected static $apiUrl = 'https://www.delivery-auto.com/api/v4/Public/';


    public static function update()
    {
        self::updateCities();
        self::updateWarehouses();


    }

    public static function updateCities()
    {
        $locales = config('laravel-delivery.locales',[]);
        if (isset($locales['uk'])) {
            $response = Http::get(self::$apiUrl.'GetAreasList', [
                'culture' => 'uk-UA',
                'country' => 1,
            ]);

            $status = false;
            if($response->getStatusCode() == 200) {

                foreach($response->json()['data'] as $city) {
                    DeliveryCity::firstOrCreate([ 'key' => $city['id'], 'type'  => 'Delivery'])
                        ->translations()->updateOrCreate(['locale' => $locales['uk']],['title' => $city['name']]);
                }
                $status = true;
            }
        }
        if (isset($locales['ru'])) {
            $response = Http::get(self::$apiUrl.'GetAreasList', [
                'culture' => 'ru-RU',
                'country' => 1,
            ]);
            $status = false;
            if($response->getStatusCode() == 200) {
                foreach($response->json()['data'] as $city) {
                    DeliveryCity::firstOrCreate([ 'key' => $city['id'], 'type'  => 'Delivery'])
                        ->translations()->updateOrCreate(['locale' => $locales['ru']],['title' => $city['name']]);
                }
                $status = true;
            }
        }

        return [
            'status' => $status,
        ];
    }

    public static function updateWarehouses()
    {
        $locales = config('laravel-delivery.locales',[]);

        foreach ($locales as $key=>$locale) {
            $loc = [];
            $loc['ref'] = ($key == 'uk') ? 'uk-UA' : 'ru-RU';
            $loc['name'] = $locale;
            self::runWarehouseUpdate($loc);
        }
    }


    protected static function runWarehouseUpdate($locale){
        $data = [
            'culture' => $locale['ref'],
            'country' => 1,
        ];
        $response = Http::get(self::$apiUrl.'GetWarehousesList', $data);

        $status = false;
        if($response->getStatusCode() == 200) {
            $key = 0;
            $keys = [];
            $data = [];
            foreach($response->json()['data'] as $warehouse) {
                $keys[] = $warehouse['CityId'];
                $data[] = $warehouse;
                $key++;


            }


            $cities = DeliveryCity::whereIn('key',$keys)->where('type','Delivery')->select('id','key')->get()->keyBy('key')->toArray();

            foreach ($data as $index=>$item) {

                DeliverySettlement::updateOrCreate(['city_id' => $cities[$item['CityId']]['id']],[
                    'city_id' => $cities[$item['CityId']]['id'],
                    'number' => 0,
                ])->translations()->updateOrCreate(['locale' => $locale['name']],[
                    'title'  => $item['name'].' '.$item['address'],
                ]);
            }


        }
            $status = true;


        return [
            'status' => $status,
        ];
    }
}
