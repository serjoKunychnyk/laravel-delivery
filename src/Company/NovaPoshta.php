<?php

namespace UNWEB\LaravelDelivery\Company;

use Illuminate\Support\Facades\Http;
use UNWEB\LaravelDelivery\Models\DeliveryCity;
use UNWEB\LaravelDelivery\Models\DeliverySettlement;

class NovaPoshta
{

    protected static $apiUrl = 'https://api.novaposhta.ua/v2.0/json/';

    public static function update()
    {
        self::updateCities();
        self::updateWarehouses();


    }

    public static function updateCities()
    {
        $locales    = config('laravel-delivery.locales',[]);
        
        $trigger    = true;
        $page       = 1;

        while ($trigger) {
            $data = json_decode('{
                "modelName": "Address",
                "calledMethod": "getCities",
                "methodProperties": {
                    "Page" : '.$page.',
                    "Limit": 500
                }
            }');
            $response = Http::post(self::$apiUrl, [$data]);

            $status = false;
            if($response->getStatusCode() == 200 && isset($response->json()['data']) && count($response->json()['data'])) {
                foreach($response->json()['data'] as $city) {
                    if (isset($locales['uk'])) DeliveryCity::firstOrCreate([ 'key' => $city['Ref'], 'type'  => 'NovaPoshta'])
                        ->translations()->updateOrCreate(['locale' => $locales['uk']],['title' => $city['Description']]);
                    if (isset($locales['ru'])) DeliveryCity::firstOrCreate([ 'key' => $city['Ref'], 'type'  => 'NovaPoshta'])
                        ->translations()->updateOrCreate(['locale' => $locales['ru']],['title' => $city['DescriptionRu']]);
                }
                $status = true;
            } else {
               $trigger = false;
            }
            $page++;
            sleep(5);
        }
        

        

        return [
            'status' => $status,
        ];
    }

    public static function updateWarehouses()
    {
        $locales = config('laravel-delivery.locales',[]);
        $trigger    = true;
        $page       = 1;

        while ($trigger) {
            $data = json_decode('{
                "modelName": "Address",
                "calledMethod": "getWarehouses",
                "methodProperties": {
                    "Page" : '.$page.',
                    "Limit": 500
                }
            }');
            $response = Http::post('https://api.novaposhta.ua/v2.0/json/', [$data]);
            $status = false;
            if($response->getStatusCode() == 200 && isset($response->json()[0]['data']) && count($response->json()[0]['data'])) {
                $key = 0;
                $keys = [];
                $data = [];
                foreach($response->json()[0]['data'] as $warehouse) {
                    $keys[] = $warehouse['CityRef'];
                    $data[] = $warehouse;
                    $key++;
                }
                $cities = DeliveryCity::whereIn('key',$keys)->where('type','NovaPoshta')->select('id','key')->get()->keyBy('key')->toArray();

                foreach ($data as $item) {
                    if (isset($cities[$item['CityRef']])) {
                        $settlement = DeliverySettlement::updateOrCreate([
                            'city_id'       => $cities[$item['CityRef']]['id'],
                            'number'        => $item['Number'],
                            'ref'           => $item['Ref']
                        ]);
                        if (isset($locales['uk'])) {
                            $settlement->translations()->updateOrCreate(['settlement_id' => $settlement->id,'locale' => $locales['uk']],[
                                'locale' => $locales['uk'],
                                'title'  => mb_substr($item['Description'],0,170),
                            ]);
                        }
                        if (isset($locales['ru'])) {
                            $settlement->translations()->updateOrCreate(['settlement_id' => $settlement->id,'locale' => $locales['ru']],[
                                'locale' => $locales['ru'],
                                'title'  => mb_substr($item['DescriptionRu'],0,170),
                            ]);
                        }
                    }

                }
                $status = true;
            } else {
               
                $trigger = false;
            }
            $page++;
            sleep(5);
        }

        return [
            'status' => $status,
        ];
    }
}
