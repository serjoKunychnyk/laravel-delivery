<?php

namespace UNWEB\LaravelDelivery\Controllers;

use App\Http\Controllers\Controller;
use UNWEB\LaravelDelivery\Models\DeliveryCity;
use UNWEB\LaravelDelivery\Models\DeliverySettlement;
use Illuminate\Http\Request;


class ApiController extends Controller
{
    public function getCity(Request $request)
    {
        if (!$request->locale) {
            $request->merge(['locale' => app()->getLocale()]);
        }

        if ($request->name && $request->type && $request->locale) {
            $items = DeliveryCity::where('type',$request->type)->whereHas('translations',function($query) use ($request) {
                return $query->where('title','LIKE',$request->name.'%')->where('locale',$request->locale);
            })->with('translations')->get();
            return [
                'items' => $items
            ];
        }

        return response('error',422);
    }

    public function getSettlement(Request $request)
    {
        if (!$request->locale) {
            $request->merge(['locale' => app()->getLocale()]);
        }

        if ($request->cityId && $request->name && $request->locale) {
            $items = DeliverySettlement::where('city_id',$request->cityId)->whereHas('translations',function($query) use ($request) {
                return $query->where('title','LIKE','%'.$request->name.'%')->where('locale',$request->locale);
            })->with('translations')->get();
            return [
                'items' => $items
            ];
        }
        return response('error',422);
    }
}


