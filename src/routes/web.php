<?php

use Illuminate\Support\Facades\Route;

$prefix = config('laravel-delivery.routePrefix', 'delivery');

$name = config('laravel-delivery.routesNamePrefix', 'api.delivery');
Route::prefix($prefix)->name($name)->group(function () {
    Route::any('/getCities', 'UNWEB\LaravelDelivery\Controllers\ApiController@getCity')->name('.getCity');
    Route::any('/getSettlements', 'UNWEB\LaravelDelivery\Controllers\ApiController@getSettlement')->name('.getSettlement');
});
