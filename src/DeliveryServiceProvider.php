<?php

namespace UNWEB\LaravelDelivery;

use Illuminate\Support\ServiceProvider;
use Illuminate\Console\Scheduling\Schedule;

class DeliveryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands([
            'UNWEB\\LaravelDelivery\\Commands\\UpdateDeliveries',

        ]);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->booted(function () {
            $schedule = $this->app->make(Schedule::class);


            $schedule->command('delivery:update')->dailyAt("1:00");
        });
        $this->publishes([
            __DIR__.'/config/laravel-delivery.php' => config_path('laravel-delivery.php'),
        ]);

        $this->loadRoutesFrom(__DIR__.'/routes/web.php');

        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
    }
}
