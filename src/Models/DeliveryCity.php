<?php

namespace UNWEB\LaravelDelivery\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeliveryCity extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function translations()
    {
        return $this->hasMany(DeliveryCityTranslation::class);
    }

    public function translated($locale = null)
    {
        if($locale === null) $locale = app()->getLocale();
        return $this->translations()->where('locale',$locale)->firstOrFail();
    }
}
