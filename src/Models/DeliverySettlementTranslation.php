<?php

namespace UNWEB\LaravelDelivery\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeliverySettlementTranslation extends Model
{
    use HasFactory;

    protected $guarded = [];
}
