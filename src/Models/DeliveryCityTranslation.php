<?php

namespace UNWEB\LaravelDelivery\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeliveryCityTranslation extends Model
{
    use HasFactory;
    protected $guarded = [];
}
