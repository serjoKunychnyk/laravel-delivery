<?php

namespace UNWEB\LaravelDelivery\Commands;

use App\Helpers\Deliveries\Delivery;
use App\Helpers\Deliveries\NewPost;
use App\Models\Prices;
use App\Models\Product;
use App\Models\Walet;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Mavinoo\Batch\Batch;

class UpdateDeliveries extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delivery:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct ()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle ()
    {
        $companies = config('laravel-delivery.companies',['NovaPoshta','Delivery']);

        $namespace = "UNWEB\\LaravelDelivery\\Company\\";
        foreach($companies as $company)
        {
            $class = $namespace.$company;
            $class::update();

        }
    }
}
