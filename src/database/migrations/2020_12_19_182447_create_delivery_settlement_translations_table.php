<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliverySettlementTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_settlement_translations', function (Blueprint $table) {
            $table->id();
            //$table->bigInteger('settlement_id');
            $table->string('locale',2);
            $table->string('title',190)->nullable();
            $table->string('additional',255)->nullable();
            $table->foreignId('settlement_id')->on('delivery_settlements')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_settlement_translations');
    }
}
