<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('delivery_settlements', function (Blueprint $table) {
            $table->index('city_id');
            $table->index('ref');
        });

        Schema::table('delivery_cities', function (Blueprint $table) {
            $table->index('key');
            $table->index('type');
        });
        Schema::table('delivery_city_translations', function (Blueprint $table) {
            $table->index('delivery_city_id');
        });
        Schema::table('delivery_settlement_translations', function (Blueprint $table) {
            $table->index('settlement_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
