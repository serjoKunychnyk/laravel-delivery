<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryCityTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_city_translations', function (Blueprint $table) {
            $table->id();
            $table->string('locale',2);
            $table->string('title',170)->nullable();
            $table->string('additional',255)->nullable();
            $table->timestamps();
            $table->foreignId('delivery_city_id')->on('delivery_cities')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_city_translations');
    }
}
