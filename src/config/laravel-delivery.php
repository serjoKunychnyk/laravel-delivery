<?php

return [


    'companies' => [
        'Delivery',
        'NovaPoshta'
    ],

    'locales' => [
        'uk' => 'ua',
        'ru' => 'ru'
    ],

    'routePrefix' => 'custom-delivery',
    'routesNamePrefix' => 'api.delivery',


];
